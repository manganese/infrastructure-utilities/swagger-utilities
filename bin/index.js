#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

const yargsLinkServiceCommand = (yargs) => {
  return yargs.command(
    'link-service',
    "Create a 'swagger' service symlink.",
    (yargs) => {
      yargs.option('server-config', {
        type: 'string',
        describe: 'The path to the server configuration file.',
        requiresArg: true,
      });

      yargs.option('swagger-config', {
        type: 'string',
        describe: 'The path to the Swagger documentation file.',
        requiresArg: true,
      });
    },
    (argv) => {
      const serviceTemplate = fs.readFileSync(
        path.join(__dirname, '../swagger.service')
      );
      const serviceStartScriptPath = path.join(
        __dirname,
        '../service-start.sh'
      );

      const service = serviceTemplate
        .toString()
        .replace('$SERVICE_START_SCRIPT', path.resolve(serviceStartScriptPath))
        .replace('$SERVER_CONFIGURATION_PATH', path.resolve(argv.serverConfig))
        .replace(
          '$SWAGGER_CONFIGURATION_PATH',
          path.resolve(argv.swaggerConfig)
        );

      fs.writeFileSync('/etc/systemd/system/swagger.service', service);
    }
  );
};

const yargs = require('yargs')
  .scriptName('swagger-utilities')
  .usage('$0 <command> [arguments]');

yargsLinkServiceCommand(yargs);

yargs.help().argv;
