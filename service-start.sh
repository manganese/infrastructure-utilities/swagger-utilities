#!/bin/bash
mkdir -p /etc/swagger
cp -u "$1" /etc/swagger/config.yml
cp -u "$2" /etc/swagger/api.json
swagger-standalone \
  --server-config /etc/swagger/config.yml \
  --swagger-config /etc/swagger/api.json